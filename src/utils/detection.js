function getIEVersion() {
  let rv = -1;
  if (navigator.appName == "Microsoft Internet Explorer") {
    let ua = navigator.userAgent;
    let re = new RegExp("MSIE ([0-9]{1,}[\\.0-9]{0,})");
    if (re.exec(ua) != null) rv = parseFloat(RegExp.$1);
  } else if (navigator.appName == "Netscape") {
    let ua = navigator.userAgent;
    let re = new RegExp("Trident/.*rv:([0-9]{1,}[\\.0-9]{0,})");
    if (re.exec(ua) != null) rv = parseFloat(RegExp.$1);
  }
  return rv;
}

function isIE() {
  return getIEVersion() != -1;
}

function getiOSversion() {
  if (/iP(hone|od|ad)/.test(navigator.platform)) {
    let v = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
    // return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
    return parseInt(v[1], 10);
  } else {
    return -1;
  }
}

function isiOS() {
  // return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
    return true;
  }
  return false;
}

const userDetected = {
  ieIs: isIE(),
  ieVersion: getIEVersion(),
  iosIs: isiOS(),
  iosVersion: getiOSversion()
};

export { userDetected };
