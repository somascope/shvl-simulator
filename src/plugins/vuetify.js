import Vue from "vue";
import Vuetify from "vuetify/lib";
import "vuetify/src/stylus/app.styl";

Vue.use(Vuetify, {
  iconfont: "md",
  theme: {
    "new-dark-theme": "#000",
    primary: "#003865",
    secondary: "#78be21",
    accent: "#005b9a"
  }
});
