// vue.config.js
module.exports = {
  productionSourceMap: false,
  publicPath: "./",
  transpileDependencies: [/node_modules[/\\\\]vuetify[/\\\\]/]
};
