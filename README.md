# SHVL Simulator

- This is a simulator of hearing and vision conditions.
- Built with Vue 2.X.

## Project assets

This project includes 4 video files in the assets folder. They're small in file size and are included in this repo.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
